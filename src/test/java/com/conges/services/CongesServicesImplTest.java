/*
 * Projet pour la pose de congés
 * 19/11/2022
 */
package com.conges.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.HttpClientErrorException;

import com.conges.model.PeriodeDeConges;
import com.conges.model.Salarie;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Classe de test des services
 */
public class CongesServicesImplTest {

	private CongesServicesImpl congesServices;
	private SimpleDateFormat formatter;

	@BeforeEach
	void setUp() {
		this.congesServices = new CongesServicesImpl();
		this.formatter = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	/**
	 * Test l'ajout de conges sur un meme mois
	 * @throws ParseException
	 */
	@Test
	void ajouterCongesMemeMoisTest() throws ParseException {
		Salarie salarieAvant = new Salarie("Luc");
		Date dateDebut = formatter.parse("2022-12-15");
		Date dateFin = formatter.parse("2022-12-20");
		
		Salarie salarieApres = congesServices.ajouterConges(salarieAvant, dateDebut, dateFin);
		assertThat(salarieApres.getListePeriodeDeConges().size() == 1);
	}
	
	/**
	 * Test l'ajout de conges à cheval sur 2 mois
	 * @throws ParseException
	 */
	@Test
	void ajouterCongesSurDeuxMoisTest() throws ParseException {
		Salarie salarieAvant = new Salarie("Luc");
		Date dateDebut = formatter.parse("2022-11-25");
		Date dateFin = formatter.parse("2022-12-05");
		
		Salarie salarieApres = congesServices.ajouterConges(salarieAvant, dateDebut, dateFin);
		assertThat(salarieApres.getListePeriodeDeConges().size() == 2);
	}
	
	/**
	 * Test que la date de début doit être avant la date de fin.
	 * @throws ParseException
	 */
	@Test
	void ajouterCongesDatesKoTest() throws ParseException {
		Salarie salarieAvant = new Salarie("Luc");
		Date dateDebut = formatter.parse("2022-12-25");
		Date dateFin = formatter.parse("2022-12-05");
		
		Exception exception = assertThrows(HttpClientErrorException.class, () -> {
			congesServices.ajouterConges(salarieAvant, dateDebut, dateFin);
	    });
	    assertThat(exception.getMessage().equals("La date de début doit être avant la date de fin."));
	}
	
	
	/**
	 * Test que le service ramène bien la période du mois demandé
	 * @throws ParseException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	@Test
	void getPeriodesDeCongesDuMoisAvecCongesTest() throws ParseException, IOException, URISyntaxException {
		Date mois = formatter.parse("2022-12-01");
		Salarie salarie = getSalarieFromJsonFile("salarie.json");
		List<PeriodeDeConges> listeCongesDuMois = congesServices.recupererPeriodesDeCongesDuMois(salarie, mois);

		assertThat(salarie.getListePeriodeDeConges().size() == 2);
		assertThat(listeCongesDuMois.size() == 1);

		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(mois);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(listeCongesDuMois.get(0).getDateDebut());
		assertThat(cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH));
	}

	/**
	 * Test que le service en cas d'absence de congés
	 * @throws ParseException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	@Test
	void getPeriodesDeCongesDuMoisSansCongesTest() throws ParseException, IOException, URISyntaxException {
		Date mois = formatter.parse("2022-12-01");
		Salarie salarie = new Salarie("Bob");
		List<PeriodeDeConges> listeCongesDuMois = congesServices.recupererPeriodesDeCongesDuMois(salarie, mois);

		assertThat(listeCongesDuMois).isNull();
	}

	/**
	 * Construit un objet Salarie à partir d'un fichier json
	 * 
	 * @param jsonFileName
	 * @return un salarie
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	private Salarie getSalarieFromJsonFile(String jsonFileName) throws URISyntaxException, IOException {
		URL resource = getClass().getClassLoader().getResource(jsonFileName);
		File file = new File(resource.toURI());
		String salarieStr = Files.readString(file.toPath(), StandardCharsets.UTF_8);
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(salarieStr, Salarie.class);
	}
}

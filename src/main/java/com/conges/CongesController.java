/*
 * Projet pour la pose de congés
 * 19/11/2022
 */
package com.conges;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.conges.model.AjouterCongesRequest;
import com.conges.model.AjouterCongesResponse;
import com.conges.model.PeriodeDeConges;
import com.conges.model.RecupererPeriodesDeCongesDuMoisRequest;
import com.conges.model.RecupererPeriodesDeCongesDuMoisResponse;
import com.conges.services.ICongesServices;

@RestController
@RequestMapping
public class CongesController {

	@Autowired
	private ICongesServices congesServices;

	/**
	 * Poser des congés pour un salarie La période de congés pourra être découpée
	 * pour créer une période de congés par période mensuelle
	 */
	@PostMapping("/recupererconges")
	public RecupererPeriodesDeCongesDuMoisResponse recupererPeriodesDeCongesDuMois(
			@RequestBody @Valid RecupererPeriodesDeCongesDuMoisRequest request) {
		List<PeriodeDeConges> listeConges = congesServices.recupererPeriodesDeCongesDuMois(request.getSalarie(),
				request.getMois());
		RecupererPeriodesDeCongesDuMoisResponse response = new RecupererPeriodesDeCongesDuMoisResponse();
		response.setListePeriodeDeConges(listeConges);
		return response;
	}

	/**
	 * Récupérer les périodes de congés d'un salarié pour un mois donné
	 */
	@PostMapping("/ajouterconges")
	public AjouterCongesResponse ajouterConges(@RequestBody @Valid AjouterCongesRequest request) {
		AjouterCongesResponse response = new AjouterCongesResponse();
		response.setSalarie(
				congesServices.ajouterConges(request.getSalarie(), request.getDateDebut(), request.getDateFin()));
		return response;
	}

}

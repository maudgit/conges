/*
 * Projet pour la pose de congés
 * 19/11/2022
 */
package com.conges;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.conges.services.CongesServicesImpl;
import com.conges.services.ICongesServices;

/**
 * Lancement de l'application
 */
@SpringBootApplication
public class CongesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CongesApplication.class, args);
	}
	
	@Bean
	public ICongesServices congesServices() {
		return new CongesServicesImpl();
	}
}

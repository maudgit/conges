/*
 * Projet pour la pose de congés
 * 19/11/2022
 */
package com.conges.model;

import java.util.List;

import javax.validation.constraints.NotBlank;

/**
 * Information d'un salarié
 */
public class Salarie {

	@NotBlank
	private String nom;
	private List<PeriodeDeConges> listePeriodeDeConges;

	public Salarie() {
	}

	public Salarie(String nom) {
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<PeriodeDeConges> getListePeriodeDeConges() {
		return listePeriodeDeConges;
	}
	public void setListePeriodeDeConges(List<PeriodeDeConges> listePeriodeDeConges) {
		this.listePeriodeDeConges = listePeriodeDeConges;
	}
		
}

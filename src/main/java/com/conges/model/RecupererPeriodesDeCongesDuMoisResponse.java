/*
 * Projet pour la pose de congés
 * 19/11/2022
 */
package com.conges.model;

import java.util.List;

/**
 * Objet Response pour recupererPeriodesDeCongesDuMois
 */
public class RecupererPeriodesDeCongesDuMoisResponse {
	
	private List<PeriodeDeConges> listePeriodeDeConges;

	public List<PeriodeDeConges> getListePeriodeDeConges() {
		return listePeriodeDeConges;
	}

	public void setListePeriodeDeConges(List<PeriodeDeConges> listePeriodeDeConges) {
		this.listePeriodeDeConges = listePeriodeDeConges;
	}

}

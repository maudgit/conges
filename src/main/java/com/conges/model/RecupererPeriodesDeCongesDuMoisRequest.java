/*
 * Projet pour la pose de congés
 * 19/11/2022
 */
package com.conges.model;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Objet Request pour recupererPeriodesDeCongesDuMois
 */
public class RecupererPeriodesDeCongesDuMoisRequest {

	@NotNull
	@Valid
	private Salarie salarie;
	@NotNull
	private Date mois;
	
	public Salarie getSalarie() {
		return salarie;
	}
	public void setSalarie(Salarie salarie) {
		this.salarie = salarie;
	}
	public Date getMois() {
		return mois;
	}
	public void setMois(Date mois) {
		this.mois = mois;
	}
	
}

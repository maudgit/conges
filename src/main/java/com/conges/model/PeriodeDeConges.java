/*
 * Projet pour la pose de congés
 * 19/11/2022
 */
package com.conges.model;

import java.util.Date;

/**
 * Période de congé sur un mois donné
 */
public class PeriodeDeConges {

	private Date dateDebut;
	private Date dateFin;

	public PeriodeDeConges() {
	}

	public PeriodeDeConges(Date dateDebut, Date dateFin) {
		super();
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}


}

/*
 * Projet pour la pose de congés
 * 19/11/2022
 */
package com.conges.model;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Objet Request pour ajouterConges
 */
public class AjouterCongesRequest {

	@NotNull
	@Valid
	private Salarie salarie;
	@NotNull
	private Date dateDebut;
	@NotNull
	private Date dateFin;

	public Salarie getSalarie() {
		return salarie;
	}

	public void setSalarie(Salarie salarie) {
		this.salarie = salarie;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	
}

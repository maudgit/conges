/*
 * Projet pour la pose de congés
 * 19/11/2022
 */
package com.conges.model;

/**
 * Objet Response pour ajouterConges
 */
public class AjouterCongesResponse {

	private Salarie salarie;

	public Salarie getSalarie() {
		return salarie;
	}

	public void setSalarie(Salarie salarie) {
		this.salarie = salarie;
	}
	
}

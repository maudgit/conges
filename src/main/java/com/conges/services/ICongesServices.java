/*
 * Projet pour la pose de congés
 * 19/11/2022
 */
package com.conges.services;

import java.util.Date;
import java.util.List;

import com.conges.model.PeriodeDeConges;
import com.conges.model.Salarie;


/**
 * Interface de services pour la gestion des congés
 */
public interface ICongesServices {

	/**
	 * Poser des congés pour un salarie
	 * La période de congés pourra être découpée pour créer une période de congés par période mensuelle
	 * @param salarie l'objet salarie avec toutes ses information
	 * @param dateDebut date de début des congés à poser
	 * @param dateFin date de fin des congés à poser
	 * @return l'objet salarie avec sa liste de congés mise à jour
	 */
	Salarie ajouterConges(Salarie salarie, Date dateDebut, Date dateFin);

	/** 
	 * Récupérer les périodes de congés d'un salarié pour un mois donné 
	 * @param salarie l'objet salarie avec toutes ses information
	 * @param mois à rechercher
	 * @return la liste de périodes de congés du mois
	 */ 
	List<PeriodeDeConges> recupererPeriodesDeCongesDuMois(Salarie salarie , Date mois);
	
}

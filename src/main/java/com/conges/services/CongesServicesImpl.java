/*
 * Projet pour la pose de congés
 * 19/11/2022
 */
package com.conges.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.HttpClientErrorException;

import com.conges.model.PeriodeDeConges;
import com.conges.model.Salarie;

/**
 * Implémentation des services pour la gestion des congés
 */
public class CongesServicesImpl implements ICongesServices {

	/**
	 * Poser des congés pour un salarie. La période de congés pourra être découpée
	 * pour créer une période de congés par période mensuelle
	 */
	@Override
	public Salarie ajouterConges(Salarie salarie, Date dateDebut, Date dateFin) {
		// Test dateDebut < dateFin
		if (dateDebut.after(dateFin)) {
			throw new HttpClientErrorException(HttpStatus.BAD_REQUEST,
					"La date de début doit être avant la date de fin.");
		}

		List<PeriodeDeConges> listePeriodeDeConges = salarie.getListePeriodeDeConges() == null
				? new ArrayList<PeriodeDeConges>()
				: salarie.getListePeriodeDeConges();

		ajouterLesPeriodesDeCongesParMois(dateDebut, dateFin, listePeriodeDeConges);

		salarie.setListePeriodeDeConges(listePeriodeDeConges);

		return salarie;
	}

	/**
	 * Ajouter les periodes de conges en les découpant par mois
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @param listePeriodeDeConges à completer
	 */
	private void ajouterLesPeriodesDeCongesParMois(Date dateDebut, Date dateFin,
			List<PeriodeDeConges> listePeriodeDeConges) {
		// Pour la période on prend :
		// - la date de début
		// - soit la date de fin soit la date de fin de mois
		boolean periodeAAjouter = true;
		Date dateDebutPeriode = dateDebut;
		Date dernierJourDuMois = getDernierJourDuMois(dateDebut);
		Date dateFinPeriode = dernierJourDuMois.before(dateFin) ? dernierJourDuMois : dateFin;

		while (periodeAAjouter) {
			listePeriodeDeConges.add(new PeriodeDeConges(dateDebutPeriode, dateFinPeriode));

			// Si la date de fin de période est avant la date de fin de congé
			// on redéfinie les dates de la période suivante à ajouter
			if (dateFinPeriode.before(dateFin)) {
				dateDebutPeriode = getPremierJourDuMoisSuivant(dateFinPeriode);
				dernierJourDuMois = getDernierJourDuMois(dateDebutPeriode);
				dateFinPeriode = dernierJourDuMois.before(dateFin) ? dernierJourDuMois : dateFin;
			} else {
				periodeAAjouter = false;
			}
		}
	}

	/**
	 * Retourne le dernier jour du mois en cours
	 * 
	 * @param date une date dans le mois
	 * @return date du dernier jour du mois
	 */
	private static Date getDernierJourDuMois(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
		return cal.getTime();
	}

	/**
	 * Retourne le premier jour du mois suivant
	 * 
	 * @param dateDernierJourDuMois du dernier jour du mois
	 * @return le premier jour du mois suivant
	 */
	private static Date getPremierJourDuMoisSuivant(Date dateDernierJourDuMois) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateDernierJourDuMois);
		cal.add(Calendar.DATE, 1);
		return cal.getTime();
	}

	/**
	 * Récupérer les périodes de congés d'un salarié pour un mois donné
	 */
	@Override
	public List<PeriodeDeConges> recupererPeriodesDeCongesDuMois(Salarie salarie, Date mois) {
		if (!CollectionUtils.isEmpty(salarie.getListePeriodeDeConges())) {
			return salarie.getListePeriodeDeConges().stream()
					.filter(periode -> isMemeMois(periode.getDateDebut(), mois)).toList();
		} else {
			return null;
		}
	}

	/**
	 * Verifie si 2 dates sont du même mois et de la même année
	 * 
	 * @param date1
	 * @param date2
	 * @return true or flase
	 */
	private static boolean isMemeMois(Date date1, Date date2) {
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTime(date1);
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(date2);
		return calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)
				&& calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH);
	}

}
